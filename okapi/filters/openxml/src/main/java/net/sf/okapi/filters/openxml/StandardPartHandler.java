package net.sf.okapi.filters.openxml;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.zip.ZipEntry;
import net.sf.okapi.common.Event;
import net.sf.okapi.common.LocaleId;

/**
 * The default part handler for OpenXMLContentFilter-based processing.
 */
public class StandardPartHandler extends ContentFilterBasedPartHandler {
	private boolean squishable;

	public StandardPartHandler(OpenXMLContentFilter contentFilter, ConditionalParameters cparams,
				OpenXMLZipFile zipFile, ZipEntry entry, boolean squishable) {
		super(contentFilter, cparams, zipFile, entry);
		this.squishable = squishable;
	}

	@SuppressWarnings("resource")
	@Override
	public Event open(String docId, String subDocId, LocaleId srcLang) throws IOException {
		InputStream isInputStream = new BufferedInputStream(zipFile.getInputStream(entry));
		if (squishable) {
			isInputStream = contentFilter.combineRepeatedFormat(isInputStream);
		}
		return openContentFilter(isInputStream, docId, subDocId, srcLang);
	}

	@Override
	public Event next() {
		Event e = super.next();
		if (e.isEndDocument() && !cparams.getTranslateWordHidden()) {
			// Update the mined styles - this is hacky
			cparams.tsExcludeWordStyles.addAll(contentFilter.getTsExcludeWordStyles());
		}
		return e;
	}
}

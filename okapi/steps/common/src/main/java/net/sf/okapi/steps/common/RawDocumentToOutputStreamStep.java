/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.steps.common;

import java.io.OutputStream;

import net.sf.okapi.common.Event;
import net.sf.okapi.common.StreamUtil;
import net.sf.okapi.common.UsingParameters;
import net.sf.okapi.common.exceptions.OkapiIOException;
import net.sf.okapi.common.pipeline.BasePipelineStep;
import net.sf.okapi.common.resource.RawDocument;

/**
 * Writes a {@link RawDocument} to an {@link OutputStream}.
 * <p>
 * WARNING: This step eats the {@link RawDocument} {@link Event} after writing its contents.
 * This step should normally be used as the last step in a pipeline when an output file is needed.
 * <p>
 * This class implements the {@link net.sf.okapi.common.pipeline.IPipelineStep}
 * interface for a step that takes a {@link RawDocument} and creates an output 
 * file from it. The generated file is passed on through a new {@link RawDocument}.
 */
@UsingParameters() // No parameters
public class RawDocumentToOutputStreamStep extends BasePipelineStep {
	private OutputStream outStream;
	
	/**
	 * Creates a new RawDocumentToOutputStreamStep object.
	 * This constructor is needed to be able to instantiate an object from newInstance()
	 */
	public RawDocumentToOutputStreamStep () {
	}
	
	// FIXME: @StepParameterMapping(parameterType = StepParameterType.OUTPUT_STREAM)
	public void setOutputStream (OutputStream outStream) {
		this.outStream = outStream;
	}
	
	@Override
	public String getDescription() {
		return "Write a RawDocument to an output stream.";
	}

	@Override
	public String getName () {
		return "RawDocument To OutputStream";
	}

	@Override
	public Event handleRawDocument (Event event) {
		try (RawDocument rawDoc = (RawDocument)event.getResource();) {				
			StreamUtil.copy(rawDoc.getStream(), outStream);				
		}
		catch ( Throwable e ) {
			throw new OkapiIOException("Error copying a RawDocument to output stream.", e);
		}
		
		// this steps writes RawDocument then eats the event
		return Event.NOOP_EVENT;
	}
}

/*===========================================================================
  Copyright (C) 2008 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
============================================================================*/

package net.sf.okapi.steps.bomconversion;

import net.sf.okapi.common.StringParameters;

public class Parameters extends StringParameters {
	private static final String REMOVEBOM = "removeBOM";
	private static final String ALSONONUTF8 = "alsoNonUTF8";
	
	public Parameters () {
		super();
	}
	
	public boolean getRemoveBOM() {
		return getBoolean(REMOVEBOM);
	}
	
	public void setRemoveBOM(boolean removeBOM) {
		setBoolean(REMOVEBOM, removeBOM);
	}
	
	public boolean getAlsoNonUTF8() {
		return getBoolean(ALSONONUTF8);
	}
	
	public void setAlsoNonUTF8(boolean alsoNonUTF8) {
		setBoolean(ALSONONUTF8, alsoNonUTF8);
	}

	public void reset() {
		super.reset();
		setRemoveBOM(false);;
		setAlsoNonUTF8(false);
	}
}

/*===========================================================================
  Copyright (C) 2009 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.connectors.crosslanguage;

import net.sf.okapi.common.ParametersDescription;
import net.sf.okapi.common.StringParameters;
import net.sf.okapi.common.uidescription.EditorDescription;
import net.sf.okapi.common.uidescription.IEditorDescriptionProvider;
import net.sf.okapi.common.uidescription.TextInputPart;

public class Parameters extends StringParameters implements IEditorDescriptionProvider {

	protected static final String SERVERURL = "serverURL";
	protected static final String USER = "user";
	protected static final String APIKEY = "apiKey";
	protected static final String PASSWORD = "password";
	
	public Parameters () {
		super();
	}
	
	public Parameters (String initialData) {
		super(initialData);
	}
	
	public String getUser () {
		return getString(USER);
	}

	public void setUser (String user) {
		setString(USER, user);
	}

	public String getApiKey () {
		return getString(APIKEY);
	}

	public void setApiKey (String apiKey) {
		setString(APIKEY, apiKey);
	}

	public String getServerURL () {
		return getString(SERVERURL);
	}

	public void setServerURL (String serverURL) {
		setString(SERVERURL, serverURL);
	}

	public String getPassword () {
		return getString(PASSWORD);
	}

	public void setPassword (String password) {
		setString(PASSWORD, password);
	}

	public void reset () {
		super.reset();
		setUser("myUsername");
		setApiKey("myApiKey");
		setServerURL("http://gateway.crosslang.com:8080/services/clGateway?wsdl");
		setPassword("");
	}

	@Override
	public ParametersDescription getParametersDescription () {
		ParametersDescription desc = new ParametersDescription(this);
		desc.add(Parameters.SERVERURL,
			"Server URL", "The full URL of the server (e.g. http://gateway.crosslang.com:8080/services/clGateway?wsdl");
		desc.add(Parameters.USER,
			"User name", "The login name to use");
		desc.add(Parameters.APIKEY,
			"API key", "The API key for the given user, engine and language pair");
		desc.add(Parameters.PASSWORD,
			"Password", "The login passowrd");
		return desc;
	}

	@Override
	public EditorDescription createEditorDescription(ParametersDescription paramsDesc) {
		EditorDescription desc = new EditorDescription("CrossLanguage MT Connector Settings");
		
		desc.addTextInputPart(paramsDesc.get(SERVERURL));
		
		desc.addTextInputPart(paramsDesc.get(USER));
		
		desc.addTextInputPart(paramsDesc.get(APIKEY));
		
		TextInputPart tip = desc.addTextInputPart(paramsDesc.get(PASSWORD));
		tip.setPassword(true);
		
		return desc;
	}

}

/*===========================================================================
  Copyright (C) 2014 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  This library is free software; you can redistribute it and/or modify it 
  under the terms of the GNU Lesser General Public License as published by 
  the Free Software Foundation; either version 2.1 of the License, or (at 
  your option) any later version.

  This library is distributed in the hope that it will be useful, but 
  WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser 
  General Public License for more details.

  You should have received a copy of the GNU Lesser General Public License 
  along with this library; if not, write to the Free Software Foundation, 
  Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307 USA

  See also the full LGPL text here: http://www.gnu.org/copyleft/lesser.html
===========================================================================*/

package net.sf.okapi.lib.transifex;

import net.sf.okapi.common.LocaleId;

public class ManualTry {

	public static void main (String args[]) {
		TransifexClient cli = new TransifexClient("https://www.transifex.com/api/2");
		cli.setCredentials("archi", "PASSWORDHERE");

//		cli.setProject("Icaria2");
//		System.out.println(cli.getHost());
//		Object[] res = cli.getResourceList(LocaleId.fromString("en-us"));
//		System.out.println(res[0]);
//		System.out.println(res[1]);
//		System.out.println(res[2]);
		
//		String[] res2 = cli.createProject("testProject", "testProject", "desc-short",
//			LocaleId.ENGLISH, false, "http://okapi.opentag.com");
//		System.out.println(res2[0]);

		cli.setProject("testProject");
		String poPath = "C:\\Users\\ysavourel\\Desktop\\MQ\\testpo.po";
//		String[] res3 = cli.putSourceResource(poPath, LocaleId.ENGLISH, "thePOFile");
//		System.out.println(res3[0]);
		poPath = "C:\\Users\\ysavourel\\Desktop\\MQ\\testpo_fr.po";
		String[] res4 = cli.putTargetResource(poPath, LocaleId.FRENCH, "thePOFile", "thePOFile");
		System.out.println(res4[0]);
	}
	
}

package net.sf.okapi.lib.beans.v2;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import net.sf.okapi.common.NSContextManager;
import net.sf.okapi.common.Util;
import net.sf.okapi.common.exceptions.OkapiException;
import net.sf.okapi.filters.idml.IDMLSkeleton;
import net.sf.okapi.filters.idml.NodeReference;
import net.sf.okapi.lib.persistence.BeanMap;
import net.sf.okapi.lib.persistence.IPersistenceSession;
import net.sf.okapi.lib.persistence.PersistenceBean;
import net.sf.okapi.lib.persistence.beans.FactoryBean;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSInput;
import org.w3c.dom.ls.LSParser;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.SAXException;

public class IDMLSkeletonBean extends PersistenceBean<IDMLSkeleton> {

	private static final String LS = "LS";
	private FactoryBean original = new FactoryBean();
	private String entry;
	private String doc;
	private String topNode;
	private String scopeNode;
	private Map<String, NodeReferenceBean> refs = new HashMap<String, NodeReferenceBean>();
	private String[] movedParts;
	
	private enum CType {
		ZF,
		ZF_ZE_D,
		N_N
	};
	
	private CType cType; // IDMLSkeleton constructor type
	
	private boolean forced = false;
	
	@Override
	protected IDMLSkeleton createObject(IPersistenceSession session) {
		switch (cType) {
		case ZF:
			return new IDMLSkeleton(original.get(ZipFile.class, session));

		case ZF_ZE_D:
			ZipFile zipFile = original.get(ZipFile.class, session);
			ZipEntry zipEntry = zipFile.getEntry(entry);			
			Document document = null;			
			try {
				document = strToDoc(doc);
			} catch (ClassNotFoundException | InstantiationException
					| IllegalAccessException | ClassCastException
					| ParserConfigurationException | SAXException
					| IOException e) {
				throw new OkapiException(e);
			}
			return new IDMLSkeleton(zipFile, zipEntry, document);
			
		case N_N:
			try {
				document = strToDoc(doc);
			} catch (ClassNotFoundException | InstantiationException
					| IllegalAccessException | ClassCastException
					| ParserConfigurationException | SAXException
					| IOException e) {
				throw new OkapiException(e);
			}
			
			XPathFactory xpf = Util.createXPathFactory();
	        XPath xp = xpf.newXPath();	        
			try {
				NamedNodeMap map = document.getFirstChild().getAttributes();
				NSContextManager ctx = new NSContextManager();
				for (int i = 0; i < map.getLength(); i++) {
					Node node = map.item(i);
					ctx.add(node.getLocalName(), node.getNodeValue());
				}
				
				xp.setNamespaceContext(ctx);
				
				Node tn = (Node) xp.evaluate("//" + topNode, document, XPathConstants.NODE);
				Node sn = (Node) xp.evaluate("//" + scopeNode, document, XPathConstants.NODE);
				return new IDMLSkeleton(tn, sn);
			} catch (XPathExpressionException e) {
				throw new OkapiException(e);
			}
			
		}
		return null;
	}

	@Override
	protected void setObject(IDMLSkeleton obj, IPersistenceSession session) {
		obj.setForced(forced);
		for (String id : refs.keySet()) {
			obj.addReferenceNode(id, refs.get(id).get(NodeReference.class, session));
		}
		obj.addMovedParts(movedParts);
	}

	private String DocToStr(Document doc) throws ClassNotFoundException, InstantiationException, IllegalAccessException, ClassCastException {
		if (doc == null) return "";
		DOMImplementationRegistry registry = 
			    DOMImplementationRegistry.newInstance();

		DOMImplementationLS impl = 
		    (DOMImplementationLS)registry.getDOMImplementation(LS);
	
        LSSerializer serializer = impl.createLSSerializer();        
        return serializer.writeToString(doc);	        
	}
	
	private Document strToDoc(String xml) throws ClassNotFoundException, InstantiationException, IllegalAccessException, ClassCastException, ParserConfigurationException, SAXException, IOException {
		DOMImplementationRegistry registry = 
			    DOMImplementationRegistry.newInstance();

		DOMImplementationLS impl = 
		    (DOMImplementationLS)registry.getDOMImplementation(LS);

		LSParser builder = impl.createLSParser(
		    DOMImplementationLS.MODE_SYNCHRONOUS, null);
			
		LSInput in = impl.createLSInput();
		in.setStringData(xml);
		return builder.parse(in);
	}
	
	/**
	 * @see http://stackoverflow.com/questions/5046174/get-xpath-from-the-org-w3c-dom-node
	 * @see lekkimworld.com/2007/06/19/building_xpath_expression_from_xml_node.html
	 */
	
	public static String getFullXPath(Node n) {
		// abort early
		if (null == n)
		  return null;

		// declarations
		Node parent = null;
		Stack<Node> hierarchy = new Stack<Node>();
		StringBuffer buffer = new StringBuffer();

		// push element on stack
		hierarchy.push(n);

		switch (n.getNodeType()) {
		case Node.ATTRIBUTE_NODE:
		  parent = ((Attr) n).getOwnerElement();
		  break;
		case Node.ELEMENT_NODE:
		  parent = n.getParentNode();
		  break;
		case Node.DOCUMENT_NODE:
		  parent = n.getParentNode();
		  break;
		default:
		  throw new IllegalStateException("Unexpected Node type" + n.getNodeType());
		}

		while (null != parent && parent.getNodeType() != Node.DOCUMENT_NODE) {
		  // push on stack
		  hierarchy.push(parent);

		  // get parent of parent
		  parent = parent.getParentNode();
		}

		// construct xpath
		Object obj = null;
		while (!hierarchy.isEmpty() && null != (obj = hierarchy.pop())) {
		  Node node = (Node) obj;
		  boolean handled = false;

		  if (node.getNodeType() == Node.ELEMENT_NODE) {
		    Element e = (Element) node;

		    // is this the root element?
		    if (buffer.length() == 0) {
		      // root element - simply append element name
		      buffer.append(node.getNodeName());
		    } else {
		      // child element - append slash and element name
		      buffer.append("/");
		      buffer.append(node.getNodeName());

		      if (node.hasAttributes()) {
		        // see if the element has a name or id attribute
		        if (e.hasAttribute("id")) {
		          // id attribute found - use that
		          buffer.append("[@id='" + e.getAttribute("id") + "']");
		          handled = true;
		        } else if (e.hasAttribute("name")) {
		          // name attribute found - use that
		          buffer.append("[@name='" + e.getAttribute("name") + "']");
		          handled = true;
		        }
		      }

		      if (!handled) {
		        // no known attribute we could use - get sibling index
		        int prev_siblings = 1;
		        Node prev_sibling = node.getPreviousSibling();
		        while (null != prev_sibling) {
		          if (prev_sibling.getNodeType() == node.getNodeType()) {
		            if (prev_sibling.getNodeName().equalsIgnoreCase(
		                node.getNodeName())) {
		              prev_siblings++;
		            }
		          }
		          prev_sibling = prev_sibling.getPreviousSibling();
		        }
		        buffer.append("[" + prev_siblings + "]");
		      }
		    }
		  } else if (node.getNodeType() == Node.ATTRIBUTE_NODE) {
		    buffer.append("/@");
		    buffer.append(node.getNodeName());
		  }
		}
		// return buffer
		return buffer.toString();
		} 
	
	@Override
	protected void fromObject(IDMLSkeleton obj, IPersistenceSession session) {
		// Determine needed IDMLSkeleton constructor type
		if (obj.getTopNode() != null && 
				obj.getScopeNode() != null &&
				obj.getTopNode().getOwnerDocument() ==
				obj.getScopeNode().getOwnerDocument()) {
			cType = CType.N_N;
		}
		else if (obj.getOriginal() != null && 
				obj.getEntry() != null && 
				obj.getDocument() != null) {
			cType = CType.ZF_ZE_D;
		}
		else {
			cType = CType.ZF;
		}
		
		switch (cType) {
		case ZF:
			original.set(obj.getOriginal(), session);
			break;
			
		case ZF_ZE_D:
			original.set(obj.getOriginal(), session);
			ZipEntry ze = obj.getEntry();
			if (ze != null) {
				entry = ze.getName();
			}
			
			try {
				doc = DocToStr(obj.getDocument());
			} catch (ClassNotFoundException | InstantiationException
					| IllegalAccessException | ClassCastException e) {
				throw new OkapiException(e);
			}
			break;
		
		case N_N:
			topNode = getFullXPath(obj.getTopNode());
			scopeNode = getFullXPath(obj.getScopeNode());
			
			try {
				doc = DocToStr(obj.getTopNode().getOwnerDocument());
			} catch (ClassNotFoundException | InstantiationException
					| IllegalAccessException | ClassCastException e) {
				throw new OkapiException(e);
			}
			break;
			
		default:
			break;
		}
		
		BeanMap.set(refs, NodeReferenceBean.class, obj.getReferences(), session);
		movedParts = obj.getMovedParts();
	}
	
	public FactoryBean getOriginal() {
		return original;
	}

	public void setOriginal(FactoryBean original) {
		this.original = original;
	}

	public String getEntry() {
		return entry;
	}

	public void setEntry(String entry) {
		this.entry = entry;
	}

	public boolean isForced() {
		return forced;
	}

	public void setForced(boolean forced) {
		this.forced = forced;
	}

	public String getDoc() {
		return doc;
	}

	public void setDoc(String doc) {
		this.doc = doc;
	}

	public String getTopNode() {
		return topNode;
	}

	public void setTopNode(String topNode) {
		this.topNode = topNode;
	}

	public String getScopeNode() {
		return scopeNode;
	}

	public void setScopeNode(String scopeNode) {
		this.scopeNode = scopeNode;
	}

	public CType getCType() {
		return cType;
	}

	public void setCType(CType cType) {
		this.cType = cType;
	}

	public Map<String, NodeReferenceBean> getRefs() {
		return refs;
	}

	public void setRefs(Map<String, NodeReferenceBean> refs) {
		this.refs = refs;
	}

	public String[] getMovedParts() {
		return movedParts;
	}

	public void setMovedParts(String[] movedParts) {
		this.movedParts = movedParts;
	}

}
